/**
 *  Copyright 2017, 2018 Hewlett Packard Enterprise Development Company, L.P.
 */
package dposconnector.restapi.types;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Represents the Data Protected Object
 * <p>
 * Implements the accessor methods for the four elements of a DPO: metadata,
 * dsa, cti and hash
 * </p>
 * 
 * @author Wenjun Fan, Joanna Ziembicka
 * @author University of Kent
 *
 */

@Document(collection = "dpoCollection")
public class DPO {
	// @Id private String id;
	// private JSONObject dpoMetadataJS;
	private byte[] metadata;
	private byte[] dsa;
	private byte[] cti;
	private byte[] hash;

	public byte[] getCti() {
		return cti;
	}

	public void setCti(byte[] cti) {
		this.cti = cti;
	}

	public byte[] getHash() {
		return hash;
	}

	public void setHash(byte[] hash) {
		this.hash = hash;
	}

	public byte[] getDsa() {
		return dsa;
	}

	public void setDsa(byte[] dsa) {
		this.dsa = dsa;
	}

	public byte[] getMetadata() {
		return metadata;
	}

	public void setMetadata(byte[] metadata) {
		this.metadata = metadata;
	}

}
