/**
 *  Copyright 2017, 2018 Hewlett Packard Enterprise Development Company, L.P.
 */
package dposconnector.restapi.types;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Manages the XML representation of a Data Sharing Agreement
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */

@Document(collection = "dpoCollection")
public class DSAXML {
    
	@Id private String id; 
	private byte[] dsaXML;
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	
	public byte[] getDsaXML() {
		return dsaXML;
	}
	public void setDsaXML(byte[] dsaXML) {
		this.dsaXML = dsaXML;
	}
	
}
