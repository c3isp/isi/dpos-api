/**
 *  Copyright 2017, 2018 Hewlett Packard Enterprise Development Company, L.P.
 */
package dposconnector.restapi.types;

import java.util.ArrayList;

import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**
 * Represents the Data Protected Object metadata set
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */
@Document(collection = "dpoCollection")
public class DPOMetadata {     		
	
	@Id private String id;
	private JSONObject dpoMetadataJS;
	private boolean isValid;
	private String validationErrors;
	
	/**
	 * Constructor
	 * <p>Creates an empty DPOMetadata object, which is valid by default</p>
	 */
	public DPOMetadata() {
		isValid = true;
		validationErrors = "";
	}
	
	/**
	 * Reports whether this metadata is valid with respect to its ontology
	 * @return <code>true</code> if this set of metadata is valid, <code>false</code> otherwise
	 */
	public Boolean isValid() {
		return isValid;
	}
	
	/**
	 * Sets the validity status of this set of metadata
	 * @param validity <code>true</code> if valid, <code>false</code> if not
	 */
	public void setValidity(boolean validity) {
		isValid = validity;
	}
	/** 
	 * Gets all the errors reported during validation of this set of metadata
	 * @return a <code>String</code> containing all the errors reported during validation of this set of metadata
	 */
	public String getValidationErrors() {
		return validationErrors;
	}
	
	/** 
	 * Sets the metadata validation errors all at once
	 * @param errors a <code>String</code> containing all the errors reported during validation of this set of metadata
	 */
	public void setValidationErrors(String errors) {
		validationErrors = errors;
	}
	
	/** Adds a validation error
	 * @param error a <code>String</code> containing an error reported during validation of this set of metadata
	 */
	public void addValidationError(String error) {
		if (validationErrors.isEmpty()) {
			validationErrors += error;
		} else {
			validationErrors += "\n";
			validationErrors += error;
		}
	}
	
	/**
	 * Gets the unique identifier associated with this set of metadata
	 * @return a unique id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the unique identifier associated with this set of metadata
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Retrieves the <code>JSONObject</code> representation of this set of metadata
	 * @return the <code>JSONObject</code> representation of this set of metadata
	 */
	public JSONObject getDpoMetadataJS() {
		return dpoMetadataJS;
	}
	
	/**
	 * Sets the internal <code>JSONObject</code> representation of this set of metadata
	 * @param dpoMetadataJS the <code>JSONObject</code> representation of this set of metadata
	 */
	public void setDpoMetadataJS(JSONObject dpoMetadataJS) {
		this.dpoMetadataJS = dpoMetadataJS;
	}
	
	/** 
	 * Retrieves the DPO ID associated with this set of metadata 
	 * @return a <code>String</code> representaion of the DPO ID
	 */
	public String getDPOId() {
		if (this.dpoMetadataJS.has("id")) {
			return this.dpoMetadataJS.getString("id");
		} else {
			return null;
		}
	}
}
