/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
/**
 * 
 */
package dposconnector.restapi.types;

import org.json.JSONArray;
import java.util.LinkedList;

/**
 * Represents the query to be executed on the Data Protected Object Storage
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */
public class DPOQuery {
	private LinkedList<String> validationErrors;
	private boolean isValid;
	private JSONArray criteria;
	private String combiningRule;
	

	public DPOQuery() {
		isValid = true;
		validationErrors = new LinkedList<String>();
		combiningRule = "and";
		criteria = new JSONArray();
	}
	
	/** 
	 *Reports whether this query is valid with respect to syntax and its ontology
	 * @return <code>true</code> if this query is valid, <code>false</code> otherwise
	 */
	public boolean isValid() {
		return isValid;
	}
	
	/**
	 * Retrieves the combining rule for this query 
	 * @return <code>and</code> if it's a CNF query, <code>or</code> if it's a DNF query
	 */
	public String getCombiningRule() {
		return combiningRule;
	}
	
	/**
	 * Sets the combining rule for this query
	 * <p>The combining rule determines whether the query's search criteria are combined with <code>AND</code> or <code>OR</code></p>
	 * @param rule <code>and</code> if it's a CNF query, <code>or</code> if it's a DNF query
	 */
	public void setCombiningRule(String rule) {
		combiningRule = rule;
	}
	
	/**
	 * Sets the validity of the query in context of syntax and/or its ontology
	 * @param validity <code>true</code> if this query is valid, <code>false</code> otherwise
	 */
	public void setValidity(boolean validity) {
		isValid = validity;
	}
	
	/**
	 * Populates the search criteria of this query object
	 * @param newcriteria a <code>JSONArray</code> of search criteria in JSONObject format
	 */
	public void setCriteria(JSONArray newcriteria) {
		criteria = newcriteria;
	}
	
	/**
	 * Retrieves the search criteria of this query object
	 * @return a <code>JSONArray</code> of search criteria in JSONObject format
	 */
	public JSONArray getCriteria() {
		return criteria;
	}
	
	/**
	 * Retrieves the list of errors encountered during query validation
	 * @return a <code>LinkedList</code> of validation errors
	 */
	public LinkedList<String> getValidationErrors() {
		return validationErrors;
	}
	
	/**
	 * Retrieves the list of errors encountered during query validation, as a formatted string
	 * @return a String containing validation errors
	 */
	public String getValidationErrorsAsString() {
		String errors = "";
		for (int i=0; i<validationErrors.size(); i++) {
			errors = errors + validationErrors.get(i) + "\n";
		}
		return errors;
	}
	
	/**
	 * Adds a validation errir
	 * @param error a <code>String</code> containing an error reported during validation of this set of metadata
	 */
	public void addValidationError(String error) {
		validationErrors.add(error);
	}
	
}
