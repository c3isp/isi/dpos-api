/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dposconnector.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

//import static org.springframework.data.mongodb.core.query.Criteria.where;
//import static org.springframework.data.mongodb.core.query.Update.update;
//import static org.springframework.data.mongodb.core.query.Query.query;

//import org.json.JSONArray;
//import org.json.JSONObject;

import dposconnector.restapi.types.DPOMetadata;
import dposconnector.restapi.types.DPOQuery;
import dposconnector.restapi.impl.DPOMetadataRepositoryCustom;

/**
 * Implements the DPO Metadata Repository interface in a MongoDB repository
 * <p>In addition to the basic Repository methods, adds custom functionality for handling DPOS queries
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */
@RepositoryRestResource
public class DPOMetadataRepositoryImpl implements DPOMetadataRepositoryCustom {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(DPOMetadataRepository.class);
	
	/** parseQuery
	 * 
	 * Translates a CNF/DNF JSON query into a MongoDB query object
	 * <p>Assumes that the JSON query has been validated, and contains the following fields:
	 * Top level: "combining_rule" and "criteria"
	 * For each criteria: "attribute", "operator", "value" and "valueType"
	 * ("valueType" is added based on the metadata/query ontology file) </p>
	 * @param mquery	a <code>DPOQuery</code> object containing the DPO query
	 * @return a mongodb Query object containing the translated query 
	 */
	
	private Query parseQuery(DPOQuery mquery) {
		Query query = new Query();
		
		//TODO: Possibly move this to the DPOQuery object?
		
		String METADATA_PREFIX = "dpoMetadataJS.map"; // the prefix needed to access the JSON structure in the database
		
		LOGGER.info("The JSON query is: " + mquery.toString());
		
		// translate the criteria in the JSON file to MongoDB query criteria
		JSONArray mcriteria = mquery.getCriteria();
		
		// Get the combining rule
		String combining_rule = mquery.getCombiningRule();
		
		// Parse out individual criteria
		List<Criteria> criteriaList = new ArrayList<Criteria>();

		for (int i=0; i<mcriteria.length(); i++) {
			JSONObject mc = mcriteria.getJSONObject(i);
			String attribute = mc.getString("attribute");
			String operator = mc.getString("operator");
			String value = mc.getString("value");
			String valueType = mc.getString("valueType");
			
			String ARRAY_SUFFIX ="";
			if (valueType.equals("array")) {
				ARRAY_SUFFIX = ".myArrayList";
			}
			
			if (operator.equals("eq") || operator.equals("in")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).is(value));
			} else if (operator.equals("ne") || operator.equals("nin")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).ne(value));
			} else if (operator.equals("lt")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).lt(value));
			} else if (operator.equals("lte")) {	
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).lte(value));
			} else if (operator.equals("gt")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).gt(value));
			} else if (operator.equals("gte")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).gte(value));
			}
		} 
		
		// Combine the criteria with the combining rules
		Criteria queryCriteria = new Criteria();
		if (combining_rule.equals("and")) {
			queryCriteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		} else if (combining_rule.equals("or")) {
			queryCriteria = new Criteria().orOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		} else { //default is "and"
			queryCriteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		}
	
		// Construct the query
		if (criteriaList.size() > 0) {
			query.addCriteria(queryCriteria);
		} // else, with no criteria, it's a blank query that returns everything
		// But presumably, this would've been caught during query validation
		
		return query;
	}

	/**
	 * runQuery
	 * 
	 * Runs a CNF/DNF query on the DPO metadata repository
	 * 
	 * @param metadataquery an input DPOQuery to run against the repository
	 * @return a List of DPOMetadata objects matching the query
	 */
	public List<DPOMetadata> runQuery(DPOQuery metadataquery)	{
			List<DPOMetadata> list;
			Query query = parseQuery(metadataquery);
			
			LOGGER.info("Parsed query is " + query.toString());
			list = mongoTemplate.find(query, DPOMetadata.class);
			
			return list;
	}
}
