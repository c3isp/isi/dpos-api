/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dposconnector.restapi.impl;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import dposconnector.restapi.types.DPOMetadata;
import dposconnector.restapi.types.DPOQuery;
import dposconnector.restapi.types.DPO;

@Service
@Profile("localdpos")

/**
 * Implements the "local" version of the DPOS repository.
 * <p>This version of DPOS uses the local filesystem to store CTI files, as well as their accompanying DSA and hash files.  It stores and indexes metadata separately in the DPOMetadataRepository.</p>
 * 
 * @author Wenjun Fan, Joanna Ziembicka
 * @author University of Kent
 *
 */

public class DPOSLocal implements DPOSInterface{

	@Value("${localdpos.folder}")
	private String localdpos_folder;
	
    @Autowired DPOMetadataRepository dpoMetadataRep;

    
    // TODO: handle occurrences where metadata repository is up, but CTI data repository is not, and vice-versa
    // This will prevent things persisting in metadata repository, but not in data repository, etc.
    // So, in create/delete, if one fails, roll-back the other.  Hm, it might be hard with "delete".
    @Autowired org.apache.hadoop.conf.Configuration hadoopConfiguration;
    
    private final static Logger LOGGER = LoggerFactory.getLogger(DPOSLocal.class);
    
    @Override
    public String createDPO(byte[] dsaBytes, byte[] ctiBytes, byte[] hashcodeBytes, DPOMetadata dpoMetadataObj, String cti_id) throws Exception{		       	   
    	   LOGGER.info("!!!!You are using local DPOS which locates at " + localdpos_folder);
    	   
    	   File directory = new File(localdpos_folder);//used to store the local repository
    	   if (! directory.exists()){
    	        directory.mkdir();
    	   }
    	   
    	   String path_dsa = localdpos_folder + cti_id +".dsa";
    	   String path_cti = localdpos_folder + cti_id + ".payload";
    	   String path_hash = localdpos_folder + cti_id + ".sign";
    	   
    	   File file_dsa = new File(path_dsa);
    	   if (!file_dsa.exists()) {
   			   file_dsa.createNewFile();
   		   }
    	   File file_cti = new File(path_cti);
    	   if (!file_cti.exists()) {
   			   file_cti.createNewFile();
   		   }
    	   File file_hash = new File(path_hash);
    	   if (!file_hash.exists()) {
   			   file_hash.createNewFile();
   		   }
    	   
    	   OutputStream outputStream = null;
		   try {
			   
			   outputStream = new FileOutputStream(file_dsa);
			   outputStream.write(dsaBytes);
			   outputStream.close();
			   
			   outputStream = new FileOutputStream(file_cti);
			   outputStream.write(ctiBytes);
			   outputStream.close();
			   
			   outputStream = new FileOutputStream(file_hash);
			   outputStream.write(hashcodeBytes);
			   outputStream.close();
			   
		   } catch (IOException e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
			   Exception ne = new Exception("Error storing DPO data. " + e.getMessage());
			   throw ne;
		   }
		   
		   // Writing data succeeded; now insert dpoMetadata to MongoDB
		   try {
			   DPOMetadata dM = null;
			   dM = dpoMetadataRep.insert(dpoMetadataObj);
			} catch (Exception e) {
				e.printStackTrace();
				//String dpo_Id = "Error, a DPO with this ID already exists ";
				//return dpo_Id;
				
				// If we are here, then data write succeeded, but metadata write failed.  
				// --> Roll back the data write.
				if (file_dsa.exists()) {
					file_dsa.delete();
				}

				if (file_cti.exists()) {
					file_cti.delete();
				}
				
				if (file_hash.exists()) {
					file_hash.delete();
				}
				// Create DPO failed; throw new exception 
				Exception ne = new Exception("Error storing DPO metadata. " + e.getMessage());
				throw ne;
			}
		   
    	   String dpo_id = cti_id;   	
		   return dpo_id;
	}
    
    public DPO readDPO(String dpo_id) throws Exception{
		DPO dpoObj = new DPO();
		
		DPOMetadata dpoMetadataObj = null;
		try{
			   dpoMetadataObj = dpoMetadataRep.findById(dpo_id);
			   dpoObj.setMetadata(dpoMetadataObj.getDpoMetadataJS().toString().getBytes());
		   }catch (Exception e) {
				e.printStackTrace();
				Exception ne = new Exception("Error retrieving metadata. " + e.getMessage());
				throw ne;
		}
		
		InputStream inputStream = null;
		byte[] tmp = null;
		String create_errors = ""; 
		try {
		   
		   String path_cti = localdpos_folder + dpo_id + ".payload"; 
		   File file_cti = new File(path_cti);
		   if (!file_cti.exists()) {
		      LOGGER.info("!!!!CTI File " + path_cti + " does not exist");
		      create_errors += "CTI path " + path_cti + "does not exist"; 
		   
		   }else{
			   LOGGER.info("!!!!CTI File " + path_cti + " does exist");
			   tmp = new byte[(int) file_cti.length()];
			   inputStream = new FileInputStream(file_cti);
			   inputStream.read(tmp);
			   dpoObj.setCti(tmp); 
			   inputStream.close();
		   }
		   
		   String path_dsa = localdpos_folder + dpo_id + ".dsa"; 
		   File file_dsa = new File(path_dsa);
		   if (!file_dsa.exists()) {
		      LOGGER.info("!!!!DSA File " + path_dsa + " does not exist");
		      create_errors += "DSA path " + path_dsa + "does not exist"; 
		   }else{
			   LOGGER.info("!!!!DSA File " + path_dsa + " does exist");
			   tmp = new byte[(int) file_dsa.length()];
			   inputStream = new FileInputStream(file_dsa);
			   inputStream.read(tmp);
			   dpoObj.setDsa(tmp); 
			   inputStream.close(); 
		   }
		   
		   String path_hash = localdpos_folder + dpo_id + ".sign"; 
		   File file_hash = new File(path_hash);
		   if (!file_hash.exists()) {
		      LOGGER.info("!!!!Hash File " + path_hash + " does not exist");
		      create_errors += "Hash path " + path_hash + "does not exist"; 
		   }else{
			   LOGGER.info("!!!!Hash File " + path_hash + " does exist");
			   tmp = new byte[(int) file_hash.length()];
			   inputStream = new FileInputStream(file_hash);
			   inputStream.read(tmp);
			   dpoObj.setHash(tmp); 
			   inputStream.close(); 
		   }
		   		   
	   } catch (IOException e) {
		// TODO Auto-generated catch block
		 e.printStackTrace();
		 Exception ne = new Exception("Error storing CTI data. " + create_errors + " " + e.getMessage());
		 throw ne;	
		}
		
		if (! create_errors.isEmpty()) {
			Exception ne = new Exception("Error storing DPO: " + create_errors);
			throw ne;
		}
		
		return dpoObj;
	}
    
	public void deleteDPO(String dpoId) throws Exception {
		//Configuration hadoopConfiguration = getConfiguration();
		try {
			DPOMetadata dpoMetadataObj = dpoMetadataRep.findById(dpoId);
			dpoMetadataRep.delete(dpoMetadataObj);
						
			String path_cti = localdpos_folder + dpoId + ".payload";
			File file_cti = new File(path_cti);
			if (file_cti.exists()) {
			    file_cti.delete();
			}

			String path_dsa = localdpos_folder + dpoId + ".dsa";
			File file_dsa = new File(path_dsa);
			if (file_dsa.exists()) {
			    file_dsa.delete();
			}
 
			String path_sign = localdpos_folder + dpoId + ".sign";
			File file_sign = new File(path_sign);
			if (file_sign.exists()) {
			    file_sign.delete();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.info("Error deleting DPO with id " + dpoId + " " + e.getMessage());
			Exception ne = new Exception("Error deleting DPO with id " + dpoId + " " + e.getMessage());
			throw ne;
			
		}
	}
	
	
	// This is the stub implementation
	public List<String> searchDPO(String searchString, boolean longResultFlag){
		List<String> searchResult = new ArrayList<String>();
		if(longResultFlag == false){
			List<DPOMetadata> dpoMetadataList = dpoMetadataRep.findAll();
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if(dpoMetadataList.get(i).getDpoMetadataJS()!= null){
					String id = dpoMetadataList.get(i).getId();
						searchResult.add(id);
				}				
			}		
		}else{
			List<DPOMetadata> dpoMetadataList = dpoMetadataRep.findAll();
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if (dpoMetadataList.get(i).getDpoMetadataJS() != null){
					String dpoMetadata = dpoMetadataList.get(i).getDpoMetadataJS().toString();
					searchResult.add(dpoMetadata);	
				}
			}
		}
		return searchResult;
	}
	
	public List<String> advancedSearchDPO(DPOQuery metadataquery, boolean longResultFlag) {		
		// Assume that the query has already been validated
		List<String> searchResult = new ArrayList<String>();
		List<DPOMetadata> dpoMetadataList = dpoMetadataRep.runQuery(metadataquery);

		if(longResultFlag == false){
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if(dpoMetadataList.get(i).getDpoMetadataJS() != null){
					String id = dpoMetadataList.get(i).getDPOId();
					if (null != id) {
						searchResult.add(id);
					}	
				}				
			}		
		}else{
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if((dpoMetadataList.get(i).getDpoMetadataJS() != null) && (dpoMetadataList.get(i).getDPOId() != null)){
					String dpoMetadata = dpoMetadataList.get(i).getDpoMetadataJS().toString();
					searchResult.add(dpoMetadata);	
				}
			}
		}
		return searchResult;
	}
	
	
}
