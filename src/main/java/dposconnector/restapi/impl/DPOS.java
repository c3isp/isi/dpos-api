/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dposconnector.restapi.impl;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dposconnector.restapi.types.DPOMetadata;
import dposconnector.restapi.types.DPOQuery;
import dposconnector.restapi.types.DPO;

/**
 * Represents the repository for Data Protected Object Storage 
 * @deprecated This class is not used anymore.  Replaced with the generic <code>DPOSInterface</code>, implemented by <code>DPOSLocal</code> and <code>DPOSCentral</code>
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */

@Service
public class DPOS {

    @Autowired DPOMetadataRepository dpoMetadataRep;

    
    // TODO: handle occurrences where metadata repository is up, but CTI data repository is not, and vice-versa
    // This will prevent things persisting in metadata repository, but not in data repository, etc.
    // So, in create/delete, if one fails, roll-back the other.  Hm, it might be hard with "delete".
    @Autowired org.apache.hadoop.conf.Configuration hadoopConfiguration;
    
    private final static Logger LOGGER = LoggerFactory.getLogger(DPOS.class);
	
    public String createDPO(byte[] dsaBytes, byte[] ctiBytes, byte[] hashcodeBytes, DPOMetadata dpoMetadataObj, String cti_id) throws Exception{		   
    	   Path path_dsa = new Path("/tmp/"+ cti_id +".dsa");
    	   Path path_cti = new Path("/tmp/"+ cti_id + ".payload");
    	   Path path_hash = new Path("/tmp/"+ cti_id + ".sign");
    	   //Configuration hadoopConfiguration = getConfiguration();
    	   
    	   FileSystem fs = null;
		   try {
			   fs = FileSystem.get(hadoopConfiguration);
			   
	    	   FSDataOutputStream out_dsa = fs.create(path_dsa);
	    	   out_dsa.write(dsaBytes);
	    	   out_dsa.close();
	    	   
	    	   FSDataOutputStream out_cti = fs.create(path_cti);
	    	   out_cti.write(ctiBytes);
	    	   out_cti.close();
	    	   
	    	   FSDataOutputStream out_hash = fs.create(path_hash);
	    	   out_hash.write(hashcodeBytes);
	    	   out_hash.close();
		   } catch (IOException e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
			   Exception ne = new Exception("Error storing DPO data. " + e.getMessage());
			   throw ne;
		   }
		   
		   // Writing data succeeded; now insert dpoMetadata to MongoDB
		   try {
			   DPOMetadata dM = null;
			   dM = dpoMetadataRep.insert(dpoMetadataObj);
			} catch (Exception e) {
				e.printStackTrace();
				//String dpo_Id = "Error, a DPO with this ID already exists ";
				//return dpo_Id;
				
				// If we are here, then data write succeeded, but metadata write failed.  
				// --> Roll back the data write.
				fs = FileSystem.get(hadoopConfiguration);
				
				if (fs.exists(path_cti)) {
				    fs.delete(path_cti, true);
				}

				if (fs.exists(path_dsa)) {
				    fs.delete(path_dsa, true);
				}
	 
				if (fs.exists(path_hash)) {
				    fs.delete(path_hash, true);
				}
				
				// Create DPO failed; throw new exception 
				Exception ne = new Exception("Error storing DPO metadata. " + e.getMessage());
				throw ne;
			}
		   
    	   String dpo_id = cti_id;   	
		   return dpo_id;
	}
    
    public DPO readDPO(String dpo_id) throws Exception{
		DPO dpoObj = new DPO();
		
		DPOMetadata dpoMetadataObj = null;
		try{
			   dpoMetadataObj = dpoMetadataRep.findById(dpo_id);
			   dpoObj.setMetadata(dpoMetadataObj.getDpoMetadataJS().toString().getBytes());
		   }catch (Exception e) {
				e.printStackTrace();
				Exception ne = new Exception("Error retrieving metadata. " + e.getMessage());
				throw ne;
		}
		
		FileSystem fileSystem = null;
		String create_errors = ""; 
		try {
		   fileSystem = FileSystem.get(hadoopConfiguration);
		   
		   String cti_file = "/tmp/" + dpo_id + ".payload"; 
		   Path cti_path = new Path(cti_file);
		   if (!fileSystem.exists(cti_path)) {
		      LOGGER.info("!!!!CTI File " + cti_file + " does not exist");
		      create_errors += "CTI path " + cti_file + "does not exist"; 
		   
		   }else{
			   LOGGER.info("!!!!CTI File " + cti_file + " does exist");
			   FSDataInputStream in = fileSystem.open(cti_path);	
			   ByteArrayOutputStream output = new ByteArrayOutputStream();
			   byte[] tmp = new byte[24];
			   while (in.read(tmp) > 0) {
				      output.write(tmp);
			   }  
			   byte[] out_cti = output.toByteArray();
			   dpoObj.setCti(out_cti); 
		   }
		   
		   String dsa_file = "/tmp/" + dpo_id + ".dsa"; 
		   Path dsa_path = new Path(dsa_file);
		   if (!fileSystem.exists(dsa_path)) {
		      LOGGER.info("!!!!DSA File " + dsa_file + " does not exist");
		      create_errors += "DSA path " + dsa_file + "does not exist"; 
		   }else{
			   LOGGER.info("!!!!DSA File " + dsa_file + " does exist");
			   FSDataInputStream in = fileSystem.open(dsa_path);	
			   ByteArrayOutputStream output = new ByteArrayOutputStream();
			   byte[] tmp = new byte[24];
			   while (in.read(tmp) > 0) {
				      output.write(tmp);
			   }  
			   byte[] out_dsa = output.toByteArray();
			   dpoObj.setDsa(out_dsa); 
		   }
		   
		   String hash_file = "/tmp/" + dpo_id + ".sign"; 
		   Path hash_path = new Path(hash_file);
		   if (!fileSystem.exists(hash_path)) {
		      LOGGER.info("!!!!Hash File " + hash_file + " does not exist");
		      create_errors += "Hash path " + hash_file + "does not exist"; 
		   }else{
			   LOGGER.info("!!!!Hash File " + hash_file + " does exist");
			   FSDataInputStream in = fileSystem.open(hash_path);	
			   ByteArrayOutputStream output = new ByteArrayOutputStream();
			   byte[] tmp = new byte[24];
			   while (in.read(tmp) > 0) {
				      output.write(tmp);
			   }  
			   byte[] out_hash = output.toByteArray();
			   dpoObj.setHash(out_hash); 
		   }
		   		   
	   } catch (IOException e) {
		// TODO Auto-generated catch block
		 e.printStackTrace();
		 Exception ne = new Exception("Error storing CTI data. " + create_errors + " " + e.getMessage());
		 throw ne;	
		}
		
		if (! create_errors.isEmpty()) {
			Exception ne = new Exception("Error storing DPO: " + create_errors);
			throw ne;
		}
		
		return dpoObj;
	}
    
	public void deleteDPO(String dpoId) throws Exception {
		//Configuration hadoopConfiguration = getConfiguration();
		try {
			DPOMetadata dpoMetadataObj = dpoMetadataRep.findById(dpoId);
			dpoMetadataRep.delete(dpoMetadataObj);
			
			FileSystem fs = FileSystem.get(hadoopConfiguration);
			
			Path cti_path = new Path("/tmp/" + dpoId + ".payload");
			if (fs.exists(cti_path)) {
			    fs.delete(cti_path, true);
			}

			Path dsa_path = new Path("/tmp/" + dpoId + ".dsa");
			if (fs.exists(dsa_path)) {
			    fs.delete(dsa_path, true);
			}
 
			Path hash_path = new Path("/tmp/" + dpoId + ".sign");
			if (fs.exists(hash_path)) {
			    fs.delete(hash_path, true);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.info("Error deleting DPO with id " + dpoId + " " + e.getMessage());
			Exception ne = new Exception("Error deleting DPO with id " + dpoId + " " + e.getMessage());
			throw ne;
			
		}
	}
	
	
	// This is the stub implementation
	public List<String> searchDPO(String searchString, boolean longResultFlag){
		List<String> searchResult = new ArrayList<String>();
		if(longResultFlag == false){
			List<DPOMetadata> dpoMetadataList = dpoMetadataRep.findAll();
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if(dpoMetadataList.get(i).getDpoMetadataJS()!= null){
					String id = dpoMetadataList.get(i).getId();
						searchResult.add(id);
				}				
			}		
		}else{
			List<DPOMetadata> dpoMetadataList = dpoMetadataRep.findAll();
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if (dpoMetadataList.get(i).getDpoMetadataJS() != null){
					String dpoMetadata = dpoMetadataList.get(i).getDpoMetadataJS().toString();
					searchResult.add(dpoMetadata);	
				}
			}
		}
		return searchResult;
	}
	
	public List<String> advancedSearchDPO(DPOQuery metadataquery, boolean longResultFlag) {		
		// Assume that the query has already been validated
		List<String> searchResult = new ArrayList<String>();
		List<DPOMetadata> dpoMetadataList = dpoMetadataRep.runQuery(metadataquery);

		if(longResultFlag == false){
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if(dpoMetadataList.get(i).getDpoMetadataJS() != null){
					String id = dpoMetadataList.get(i).getDPOId();
					if (null != id) {
						searchResult.add(id);
					}	
				}				
			}		
		}else{
			for(int i = 0; i < dpoMetadataList.size(); i++){
				if((dpoMetadataList.get(i).getDpoMetadataJS() != null) && (dpoMetadataList.get(i).getDPOId() != null)){
					String dpoMetadata = dpoMetadataList.get(i).getDpoMetadataJS().toString();
					searchResult.add(dpoMetadata);	
				}
			}
		}
		return searchResult;
	}
	
	
}
