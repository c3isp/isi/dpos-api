/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dposconnector.restapi.impl;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import dposconnector.restapi.types.DPO;
import dposconnector.restapi.types.DPOMetadata;
import dposconnector.restapi.types.DPOQuery;

/**
 * Generic representation of the Data Protected Object Storage
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */
//public interface DPOSCrudInterface extends CrudRepository{
public interface DPOSInterface{

	/**
	 * Creates a Data Protected Object in the repository
	 * @param dsaXMLBytes a byte array containing the DSA XML file
	 * @param ctiBytes a byte array containing the CTI data 
	 * @param hashcodeBytes a byte array containing the hash code of CTI data and DSA 
	 * @param dpoMetadataObj a <code>DPOMetadata</code> object containing the DPO Metadata
	 * @param id the unique ID for this DPO 
	 * @return DPO ID
	 * @throws Exception
	 */
	String createDPO(byte[] dsaXMLBytes, byte[] ctiBytes, byte[] hashcodeBytes, DPOMetadata dpoMetadataObj, String id) throws Exception;
	//public List<DPOMetadata>runQuery(DPOQuery metadataQuery);

	/**
	 * Runs the provided query on the DPOS
	 * 
	 * @param configuredQuery a <code>DPOQuery</code> object encapsulating the parsed JSON input query
	 * @param longResultFlag TRUE for long results (full metadata entries); FALSE for short results (DPO ids only)
	 * @return a <code>List</code> of search results
	 */ 
	List<String> advancedSearchDPO(DPOQuery configuredQuery, boolean longResultFlag);

	/**
	 * Retrieves the Data Protected Object by its identifier
	 * @param dpoId the unique ID of the DPO to be retrieved
	 * @return a <code>DPO</code> object encapsulating the four CTI Bundle components
	 * @throws Exception
	 */
	DPO readDPO(String dpoId) throws Exception;

	/**
	 * Deletes a DPO by its identifier.
	 * 
	 * @param dpoId the identifier of the DPO to be deleted 
	 * @throws Exception
	 */
	
	void deleteDPO(String dpoId) throws Exception;
}