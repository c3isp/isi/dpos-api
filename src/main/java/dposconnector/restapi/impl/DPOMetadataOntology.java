/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dposconnector.restapi.impl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
//import org.apache.hadoop.conf.Configuration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;


import dposconnector.restapi.types.DPOMetadata;
import dposconnector.restapi.types.DPOQuery;

/**
 * Manages the ontology for DPO metadata
 * <p>Maintains a list of ontology attributes, their type, allowable operators and restrictions </p>
 * <p>Used to validate and interpret DPO metadata and DPO queries on input</p>
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */

@Service
public class DPOMetadataOntology {
    @Autowired DPOMetadataRepository dpoMetadataRep;
        
    private final static Logger LOGGER = LoggerFactory.getLogger(DPOMetadataOntology.class);
    //private  String DPO_ATTRIBUTE_CONFIG_FILENAME = "dpo-metadata-ontology.json";
    private final String ontology_file;
   
    private JSONObject ontology;
    private JSONObject indexed_ontology_attributes;

    
    /**
     * Converts an input date string into its equivalent in UTC timezone
     * 
     * @param inputDate a string representing a date in ISO8601 format
     * @return a string representation of inputDate in ISO8601 format, UTC timezone
     */

    private String convertToUTC(String inputDate) throws Exception{
    	String outputDate ="";
    	
    	DateTimeFormatter f = DateTimeFormatter.ISO_DATE_TIME;
    	
    	OffsetDateTime odt = OffsetDateTime.parse(inputDate, f);
    	ZonedDateTime zdt = odt.atZoneSameInstant(ZoneId.of("Z"));

    	outputDate = zdt.format(f);
    	return outputDate;
    }
 
    /** 
     * Reads in the metadata ontology file into a JSONObject
     * @return JSONObject representation of the metadata ontology
     * @throws IOException
     */
	private JSONObject readAttributeConfig() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ontology_file);
		byte[] bytes = IOUtils.toByteArray(inputStream);
		String configString = new String(bytes);
		
		// Read configuration into JSON
		JSONObject config = new JSONObject(configString);
		
		/* TODO: Add config file validation
		 * This is date constraint validation if valueType is date
		 * 
	    try {			
	    	fromDate = OffsetDateTime.parse(bottomConstraint, f);
	    	toDate = OffsetDateTime.parse(topConstraint, f);
	    } catch (Exception e) {
	    	LOGGER.error("Error: constraints in the ontology file are not in ISO8601 format");
	    	isValid = false;
			break;
	    }*/
		
		return config;
	}
    /**
     * Generates a string representation of this ontology
     * @return a JSON string representation of this ontology
     */
	public String toString() {
		String result = ontology.toString();
		return result;		
	}
	
	/** 
	 * Constructor.  Reads in ontology from the ontology config file
	 * 
	 * @param ontology_file a string representation of the ontology file
     * @throws IOException
	 */	
	public DPOMetadataOntology(@Value("${c3isp.metadata.ontology}") String ontology_file) throws IOException{
		this.ontology_file = ontology_file;
    	System.out.println("================== " + ontology_file + "================== ");

		ontology = readAttributeConfig(); // JSON representation of the metadata ontology
		indexed_ontology_attributes = new JSONObject(); // an ontology-derived map of attribute name -> attribute config

		// Build a JSON map (indexed_ontology_attributes) that can be accessed by attribute name
		// this is a local helper object
		JSONArray attributes = ontology.getJSONArray("attributes");		
		for (int i=0; i<attributes.length(); i++) {
			JSONObject attrib_config = attributes.getJSONObject(i);
			String name = attrib_config.getString("name");
			indexed_ontology_attributes.put(name, attrib_config);
		}	
	}
	
	
	/**
	 * Validates and configures input metadata 
	 * <p>Creates a new DPOMetadata object.  If input metadata is invalid, the object will be marked as such, and contain a list of errors incurred during validation.</p>
	 * @param inputJS a JSON object containing a representation of DPO metadata
	 * @return a new DPOMetadata object containing the input metadata
	 */
	public DPOMetadata configureMetadata(JSONObject inputJS) {
		DPOMetadata metadataObj = new DPOMetadata();
		JSONObject metadataJS = new JSONObject();
		
		
		metadataObj.setValidity(true);  // Set validity to true by default, switch to false below on any validation failure
		
		// Make sure that the metadata contains an id
		if (inputJS.has("id")) {
			String id = inputJS.getString("id");
			metadataObj.setId(id);
		} else {
			LOGGER.error("Error: Metadata does not contain an id field");
			metadataObj.addValidationError("Error: Metadata does not contain an id field");
			metadataObj.setValidity(false);	
		}
		
		// Iterate over all metadata attributes.  Validate and sanitize them.  
		JSONArray mkeys = inputJS.names();
		
		for (int i=0; i<mkeys.length(); i++) {
			String mkey = mkeys.getString(i);
			
			if(indexed_ontology_attributes.has(mkey)) {
				
				JSONObject attribute = indexed_ontology_attributes.getJSONObject(mkey); // the config for this attribute
				String valueType = attribute.getString("valueType");	
				
				if ("string".equals(valueType)) {
					String value = inputJS.getString(mkey);
					metadataJS.put(mkey, value);
					
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");			
						
						// Compare the string's length against the constraints
						int bottomConstraint = constraints.getInt(0);
						int topConstraint = constraints.getInt(1);
						
						if ((value.length() < bottomConstraint) || (value.length() > topConstraint) ) {
							LOGGER.error("Error: Metadata attribute " + mkey + " has size outside constraints " + constraints.toString() );
							metadataObj.addValidationError("Error: Metadata attribute " + mkey + " has size outside constraints " + constraints.toString());
							metadataObj.setValidity(false);						
						} 
					}
					
				} else if ("number".equals(valueType)) {
					float value = inputJS.getFloat(mkey);
					metadataJS.put(mkey, value);
					
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");	
						
						// Compare the number against the top and bottom constraints
						int bottomConstraint = constraints.getInt(0);
						int topConstraint = constraints.getInt(1);
						
						if ((value < bottomConstraint) || (value > topConstraint) ) {
							LOGGER.error("Error: Metadata attribute " + mkey + " is outside constraints " + constraints.toString() );
							metadataObj.addValidationError("Error: Metadata attribute " + mkey + " is outside constraints " + constraints.toString());
							metadataObj.setValidity(false);
						}
					}
					
				} else if ("array".equals(valueType)) {
					JSONArray value = inputJS.getJSONArray(mkey);
					metadataJS.put(mkey, value);
					
					// Check the size of the array against constraints
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");
						int bottomConstraint = constraints.getInt(0);
						int topConstraint = constraints.getInt(1);
						if ((value.length() < bottomConstraint) || (value.length() > topConstraint) ) {
							LOGGER.error("Error: Metadata attribute " + mkey + " has size outside constraints " + constraints.toString() );
							metadataObj.addValidationError("Error: Metadata attribute " + mkey + " has size outside constraints " + constraints.toString());
							metadataObj.setValidity(false);	
						}
					}
					
				} else if ("date".equals(valueType)) {
					String inputDateStr = inputJS.getString(mkey);
					
			    	DateTimeFormatter f = DateTimeFormatter.ISO_DATE_TIME;

			    	String value;
			    	// Parse the date string and convert it to UTC time zone
			    	try {
			    		value = convertToUTC(inputDateStr); 
			    		metadataJS.put(mkey, value);
			    	
				    	// Check the date against date-range constraints
				    	if (attribute.has("constraints")) {	
				    		JSONArray constraints = attribute.getJSONArray("constraints");
				    		
				    		OffsetDateTime inputDate = OffsetDateTime.parse(inputDateStr,f);
							OffsetDateTime bottomConstraint = OffsetDateTime.parse(constraints.getString(0),f);
							OffsetDateTime topConstraint = OffsetDateTime.parse(constraints.getString(0),f);
							
					    	if ( (inputDate.isBefore(bottomConstraint)) || (inputDate.isAfter(topConstraint)) ) {
					    		LOGGER.error("Error: Metadata attribute " + mkey + " has value outside constraints " + constraints.toString() );
								metadataObj.addValidationError("Error: Metadata attribute " + mkey + " has value outside constraints " + constraints.toString());
								metadataObj.setValidity(false);	
					    	}	
				    	}
				    	
					} catch (Exception e) {
				    	LOGGER.error("Error: " + mkey + " is not in ISO8601 format");
						metadataObj.addValidationError("Error: " + mkey + " is not in ISO8601 format");
						metadataObj.setValidity(false);	
				    }  	
				    	
				} else if ("enumerated".equals(valueType)) {	
					String value = inputJS.getString(mkey);
					metadataJS.put(mkey, value);
					
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");			
						
						// Check whether the value is in the configured enumerated list
						if (! constraints.toList().contains(value) ) {
							LOGGER.error("Error: Metadata attribute " + mkey + " is not within the allowed list" + constraints.toString() );
							metadataObj.addValidationError("Error: Metadata attribute " + mkey + " is not within the allowed list" + constraints.toString());
							metadataObj.setValidity(false);						
						} 
					}
				} else if ("boolean".equals(valueType)) {	
						String value = inputJS.getString(mkey);
						metadataJS.put(mkey, value);
						
						if (attribute.has("constraints")) {					
							JSONArray constraints = attribute.getJSONArray("constraints");			
							
							// Check whether the value is in the configured enumerated list
							if (! constraints.toList().contains(value) ) {
								LOGGER.error("Error: Metadata attribute " + mkey + " is not within the allowed list" + constraints.toString() );
								metadataObj.addValidationError("Error: Metadata attribute " + mkey + " is not within the allowed list" + constraints.toString());
								metadataObj.setValidity(false);
							} 
						}
				} else {
					// We should never reach here, since we read value types from the config file, but added for completeness/debugging
					LOGGER.error("Error: Metadata attribute " + mkey + " has an invalid valueType " + valueType );
					metadataObj.addValidationError("Error: Metadata attribute " + mkey + " has an invalid valueType " + valueType );
					metadataObj.setValidity(false);	
				}
					
			} else {
				LOGGER.error("Error: Metadata attribute " + mkeys.getString(i) + " is not in the ontology");
				metadataObj.addValidationError("Metadata attribute " + mkeys.getString(i) + " is not in the ontology");
				metadataObj.setValidity(false);	
			}
		} //end for (metadata attributes)	

		if (metadataObj.isValid()) {
			metadataObj.setDpoMetadataJS(metadataJS);
		}	
		return metadataObj;
	}
	
	
	/** 
	 * Validate metadata query against the ontology config file.  
	 * <p>Adds ontology-based configuration information to the query to prepare it for parsing.</p>
	 * @param searchString the DPO metadata query string 
	 * @return a JSONObject containing the configured metadata query
	 */
	public DPOQuery configureQuery(String searchString) {
		DPOQuery newquery = new DPOQuery();
		
		// Set the query validity to true by default before deciding otherwise
		newquery.setValidity(true);
		
		// First check if the searchString parses at all
		JSONObject metadataquery = new JSONObject();
		try {
			metadataquery = new JSONObject(searchString);
		} catch (Exception e) {
			LOGGER.error("Error: Cannot parse query into a JSON object");
			newquery.addValidationError("Error: Cannot parse query into a JSON object");
			newquery.setValidity(false);
		} 
		
		// Add combining rule
		newquery.setCombiningRule(metadataquery.optString("combining_rule", "and")); // Default is "and" if it's not specified
	
		// Add the type information from the config file into the query
		if (metadataquery.has("criteria")) {
			JSONArray criteria = metadataquery.getJSONArray("criteria");
			JSONArray newcriteria = new JSONArray();
			
			/* Iterate over the query criteria, validate each one
				Merge some of the information into the search criteria */
			for (int i=0; i<criteria.length(); i++) {
				JSONObject newcriterion = criteria.getJSONObject(i);
				
				/* Check if criterion has all the required fields: name, operator, and value */
				
				// Check for a name field
				String name = "";
				try {
					name = newcriterion.getString("attribute");
					// Check if this query attribute is in the ontology
					if (! indexed_ontology_attributes.has(name)) {
						LOGGER.error("Error: query attribute " + name + " is not in the ontology");
						newquery.addValidationError("Error: query attribute " + name +" is not in the ontology");
						newquery.setValidity(false);
						// Don't parse this query anymore
						break;
					}
				} catch (Exception e) {
					LOGGER.error("Error: query has an attribute with no name field");
					newquery.addValidationError("Error: query has an attribute with no name field");
					newquery.setValidity(false);
					// Don't parse this query anymore
					break;
				}
			
				// Check for a value field, and if it is of the correct type
				String valueType = indexed_ontology_attributes.getJSONObject(name).getString("valueType");
				newcriterion.put("valueType", valueType);
				try {
					if ("string".equals(valueType)) {
						String value = newcriterion.getString("value");
					} else if ("number".equals(valueType) ) {
						float value = newcriterion.getFloat("value");
					} else if ("array".equals(valueType)) {
						JSONArray value = newcriterion.getJSONArray("value");
					} else if ("date".equals(valueType)) {
						String value = newcriterion.getString("value");
					} else if ("enumerated".equals(valueType)) {
						String value = newcriterion.getString("value");
					} else {
						// We should never reach here, since we read value types from the config file, but added for completeness/debugging
						LOGGER.error("Error: Metadata attribute " + name + " has an invalid valueType " + valueType );
						newquery.addValidationError("Error: Metadata attribute \"" + name + "\" has an invalid valueType " + valueType );
						newquery.setValidity(false);
						break;
					}
				} catch (Exception e) {
					LOGGER.error("Error: Failed to retrieve a value of type " + valueType + " for query attribute on " + name);
					newquery.addValidationError("Error: Failed to retrieve a value of type \"" + valueType + "\" for query attribute on " + name);
					newquery.setValidity(false);
					break; // Don't parse this query anymore
				}	

				// Check if the criterion has an allowed operator 
				String operator = "";
				JSONArray allowed_operators = indexed_ontology_attributes.getJSONObject(name).getJSONArray("operators");
				try {
					operator = newcriterion.getString("operator");
					
					if (! allowed_operators.toList().contains(operator)) {
						LOGGER.error("Error: Invalid operator \"" + operator + "\" for query " + name);
						newquery.addValidationError("Error: Invalid operator \"" + operator + "\" for query " + name);
						newquery.setValidity(false);
						break;
						
					}
				} catch (Exception e) {
					LOGGER.error("Error: Failed to retrieve an operator for query attribute on " + name);
					newquery.addValidationError("Error: Failed to retrieve an operator for query attribute on " + name);
					newquery.setValidity(false);
					break; // Don't parse this query anymore
				}	
				
				
				/* If we got this far, it means that we have a valid name, operator and value for the query criterion 
				 * Operator and name are stored in variables: operator and name; we will re-read the value based on type
				 * Now, validate the constraints and sanitize values if necessary
				 */
				
				JSONObject attribute = indexed_ontology_attributes.getJSONObject(name); // the config for this attribute
			
				if ("string".equals(valueType)) {
					String value = newcriterion.getString("value");
					
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");			
						
						
						// Compare the string's length against the constraints
						int bottomConstraint = constraints.getInt(0);
						int topConstraint = constraints.getInt(1);
						
						if ((value.length() < bottomConstraint) || (value.length() > topConstraint) ) {
							LOGGER.error("Error: Metadata attribute " + name + " has size outside constraints " + constraints.toString() );
							newquery.addValidationError("Error: Metadata attribute " + name + " has size outside constraints " + constraints.toString());
							newquery.setValidity(false);						
						}
					}	
					
					
				} else if ("number".equals(valueType)) {
					float value = newcriterion.getFloat("value");
					
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");
						
						// Compare the number against the top and bottom constraints
						int bottomConstraint = constraints.getInt(0);
						int topConstraint = constraints.getInt(1);
						
						if ((value < bottomConstraint) || (value > topConstraint) ) {
							LOGGER.error("Error: Query attribute " + name + " is outside constraints " + constraints.toString() );
							newquery.addValidationError("Error: Query attribute " + name + " is outside constraints " + constraints.toString());
							newquery.setValidity(false);
						}
					}
					
				} else if ("array".equals(valueType)) {
						
					JSONArray value = newcriterion.getJSONArray("value");
							
					// Check the size of the array against constraints
					if (attribute.has("constraints")) {
						JSONArray constraints = attribute.getJSONArray("constraints");
						
						// Compare the size of array against the top and bottom constraints
						int bottomConstraint = constraints.getInt(0);
						int topConstraint = constraints.getInt(1);
						
						if ((value.length() < bottomConstraint) || (value.length() > topConstraint) ) {
							LOGGER.error("Error: Query attribute " + name + " has size outside constraints " + constraints.toString() );
							newquery.addValidationError("Error: Query attribute " + name + " has size outside constraints " + constraints.toString());
							newquery.setValidity(false);	
						}
					}
					
				} else if ("date".equals(valueType)) {
					String inputDateStr = newcriterion.getString("value");
					
			    	DateTimeFormatter f = DateTimeFormatter.ISO_DATE_TIME;

			    	String value ="";
			    	// Parse the date string and convert it to UTC time zone
			    	try {
			    		value = convertToUTC(inputDateStr);
			    		newcriterion.put("value", value);
			    	} catch (Exception e) {
				    	LOGGER.error("Error: " + name + " is not in ISO8601 format");
				    	newquery.addValidationError("Error: " + name + " is not in ISO8601 format");
				    	newquery.setValidity(false);	
				    }  		
			    	
			    	// Check the date against date-range constraints
			    	if ((! value.isEmpty()) && (attribute.has("constraints"))) {	
			    		JSONArray constraints = attribute.getJSONArray("constraints");
			    		
			    		OffsetDateTime inputDate = OffsetDateTime.parse(inputDateStr,f);
						OffsetDateTime bottomConstraint = OffsetDateTime.parse(constraints.getString(0),f);
						OffsetDateTime topConstraint = OffsetDateTime.parse(constraints.getString(0),f);
						
				    	if ( (inputDate.isBefore(bottomConstraint)) || (inputDate.isAfter(topConstraint)) ) {
				    		LOGGER.error("Error: Query attribute " + name + " has value outside constraints " + constraints.toString() );
				    		newquery.addValidationError("Error: Query attribute " + name + " has value outside constraints " + constraints.toString());
				    		newquery.setValidity(false);	
				    	}	
			    	}
				    	
				    	
				} else if ("enumerated".equals(valueType)) {	
					String value = newcriterion.getString("value");
					
					if (attribute.has("constraints")) {					
						JSONArray constraints = attribute.getJSONArray("constraints");			
						
						// Check whether the value is in the configured enumerated list
						if (! constraints.toList().contains(value) ) {
							LOGGER.error("Error: Metadata attribute " + name + " is not within the allowed list" + constraints.toString() );
							newquery.addValidationError("Error: Metadata attribute " + name + " is not within the allowed list" + constraints.toString());
							newquery.setValidity(false);						
						} 
					}
				} else {
					// We should never reach here, since we read value types from the config file, but added for completeness/debugging
					LOGGER.error("Error: Metadata attribute " + name + " has an invalid valueType " + valueType );
					newquery.addValidationError("Error: Metadata attribute " + name + " has an invalid valueType " + valueType );
					newquery.setValidity(false);	
				} // end if (valueType)
				
				newcriteria.put(newcriterion);
			} //end for (metadata attributes)	
		
			if (newquery.isValid()) {
				newquery.setCriteria(newcriteria);
			}
		} else {
			LOGGER.error("Error: query has no defined criteria" );
			newquery.addValidationError("Error: query has no defined criteria");
			newquery.setValidity(false);
		}
		return newquery;
	} //end configureQuery
		

} // end class
