/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 *  All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or
 *    without modification, are permitted provided that the following
 *    conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above
 *        copyright notice, this list of conditions and the following
 *        disclaimer in the documentation and/or other materials provided
 *        with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dposconnector.restapi.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
//import org.springframework.core.io.InputStreamResource;
//import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import dposconnector.restapi.types.DPO;
import dposconnector.restapi.types.DPOMetadata;
import dposconnector.restapi.types.DPOQuery;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponses;

@ApiModel(value = "DPOS", description = "DPOS REST APIs")
@RestController
@RequestMapping("/v1")

/**
 *
 * @author Wenjun Fan, Joanna Ziembicka
 *
 *         This is the main implementation class of the DPOS REST API
 *
 */

public class DPOSAPIImplementation {

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	// @Value("${c3isp.metadata.ontology}")
	// private String DPO_ATTRIBUTE_CONFIG_FILENAME;

	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	// private DPOSCentral dPOSCentral;
	private DPOSInterface dpos;

	@Autowired
	private DPOMetadataOntology ontology;

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
	}

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(DPOSAPIImplementation.class);

	/**
	 * createDPO
	 * <p>
	 * Persists a DPO in the DPOS
	 * </p>
	 *
	 * @param dsaFile     an XML file containing the Data Sharing Agreement attached
	 *                    to this DPO
	 * @param dpoMetadata a text file containing the JSON metadata attached to this
	 *                    DPO
	 * @param ctiFile     the data file containing CTI
	 * @param hashCode    the hash code of this DPO; used to verify the integrity of
	 *                    the DPO
	 * @return dpo_id of the created Data Protected Object
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	@ApiOperation(notes = "Persist a DPO in the DPOS.", value = "create DPO")
	@ApiResponses(value = {
			// @ApiResponse(code = 201, message = "Created"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(method = RequestMethod.POST, value = "/createDPO")
	public ResponseEntity<String> createDPO(@RequestParam("DSAFile") MultipartFile dsaFile,
			@RequestParam("DPOMetadata") MultipartFile dpoMetadata, @RequestParam("CTIFile") MultipartFile ctiFile,
			@RequestParam("HashCode") MultipartFile hashCode)
			throws ParserConfigurationException, SAXException, IOException {
		String resp;
		HttpStatus httpstatus;
		ResponseEntity<String> response;
		String ctiFilename = ctiFile.getOriginalFilename();
		LOGGER.info("cti File name： " + ctiFilename);

		byte[] dsaXMLBytes = dsaFile.getBytes();
		byte[] dpoMetadataBytes = dpoMetadata.getBytes();

//		byte[] ctiBytes = ctiFile.getBytes();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ByteArrayInputStream is = new ByteArrayInputStream(ctiFile.getBytes());
		IOUtils.copy(is, os);

		byte[] hashcodeBytes = hashCode.getBytes();

		// convert byte array to JSONObject
		JSONObject dpoMetadataJS = new JSONObject(new String(dpoMetadataBytes));
		// TODO validate the dpoMetadata
		String id = dpoMetadataJS.getString("id");
		LOGGER.info("!!!dpoMetadata id: " + id);

		// validate metadata
		// boolean isValid = ontology.validateMetadata(dpoMetadataJS);
		DPOMetadata dpoMetadataObj = ontology.configureMetadata(dpoMetadataJS);

		if (dpoMetadataObj.isValid()) {
			try {
				// TODO: add error checking in createDPO
//				resp = dpos.createDPO(dsaXMLBytes, ctiBytes, hashcodeBytes, dpoMetadataObj, id);
				resp = dpos.createDPO(dsaXMLBytes, os.toByteArray(), hashcodeBytes, dpoMetadataObj, id);
				System.out.println("response to createDPO on dpos = " + resp);
				httpstatus = HttpStatus.OK;
			} catch (Exception e) {
				httpstatus = HttpStatus.BAD_REQUEST;
				resp = e.getMessage();
			}
		} else {
			httpstatus = HttpStatus.BAD_REQUEST;
			resp = "Metadata validation returned the following errors:\n" + dpoMetadataObj.getValidationErrors();
		}
		response = new ResponseEntity<String>(resp, httpstatus);
		return response;
	}

	/**
	 * stubSearchDPO
	 *
	 * @param longResultFlag
	 * @param searchString
	 * @return
	 *//*
		 * @ApiOperation(notes =
		 * "Search the DPO based on a set of configured search fields.  Stub method that returns everything in the DB."
		 * , value = "stub search DPO")
		 *
		 * @RequestMapping(method=RequestMethod.POST,
		 * value="stubSearchDPO/{longResultFlag}") public ResponseEntity<List<String>>
		 * stubSearchDPO(@PathVariable boolean
		 * longResultFlag, @RequestBody @ApiParam(defaultValue="hello") String
		 * searchString ) {
		 *
		 * //TODO: figure out the autowired config
		 * LOGGER.info("!!!!!DEBUGGING ONTOLOGY"); LOGGER.info("Ontology is: " +
		 * ontology.toString());
		 *
		 * ResponseEntity<List<String>> response; List<String> searchResult = new
		 * ArrayList<String>(); try{ searchResult = dpos.searchDPO(searchString,
		 * longResultFlag); response = new ResponseEntity<List<String>>(searchResult,
		 * HttpStatus.OK); }catch(Exception e){ response = new
		 * ResponseEntity<List<String>>(searchResult, HttpStatus.NOT_FOUND); } //TODO
		 * return response; }
		 */

	/**
	 * searchDPO
	 * <p>
	 * Search the DPOS based on a set of configured search fields provided as DPO
	 * metadata
	 * </p>
	 *
	 * @param longResultFlag TRUE for long results; FALSE for short results
	 * @param searchString   a string JSON representation of either DNF or CNF of
	 *                       search criteria (based on the DPO ontology
	 * @return either an array list of search results (if HttpStatus.OK), or of
	 *         errors
	 */
	@ApiOperation(notes = "Search the DPOS based on a set of configured search fields provided as DPO metadata. \n\n"
			+ " The input is a string JSON representation of either DNF or CNF of search criteria (based on the DPO ontology), and a boolean value, TRUE for long results and FALSE for short results.\n\n"
			+ " The return will be an array of strings in JSON format containing the search fields associated with an individual DPO (when boolean is TRUE) or an array of DPO ids as strings (when boolean is FALSE).\n\n"
			+ "Example input: " + " \n\n" + "	{ \n" + "		\"combining_rule\": \"and\",\n"
			+ "		\"criteria\": [\n" + "		{\n" + "			\"attribute\": \"event_type\",\n"
			+ "			\"operator\": \"eq\",\n" + "			\"value\": \"Firewall Event\"\n" + "		},	\n"
			+ "		{\n" + "			\"attribute\": \"start_time\",\n" + "			\"operator\": \"gt\",\n"
			+ "			\"value\": \"2017-12-12T12:00:00.0Z\"\n" + "		},	\n" + "		{\n"
			+ "			\"attribute\": \"end_time\",\n" + "			\"operator\": \"lt\",\n"
			+ "			\"value\": \"2017-12-16T18:00:00.0Z\"\n" + "		}\n" + "		]\n" + "	}\n\n" + ""
			+ "Example output: " + " \n\n" + "	If longResultFlag is true:\n" + "	[ \n"
			+ "		{\"start_time\":\"2017-12-12T13:00:00Z\", \"event_type\":\"Firewall Event\", \"organization\":\"3DRepo\", \"end_time\":\"2017-12-12T13:59:00Z\", \"id\":\"2001\", \"dsa_id\":\"dsa-1\"}\",\n"
			+ "		{\"start_time\":\"2017-12-12T14:00:00Z\", \"event_type\":\"Firewall Event\", \"organization\":\"3DRepo\", \"end_time\":\"2017-12-12T14:59:00Z\", \"id\":\"2002\", \"dsa_id\":\"dsa-1\"}\",\n"
			+ "	] \n\n" + "	If longResultFlag is false:\n" + "	[\n" + "		\"2001\",\n" + "		\"2002\"\n"
			+ "	] \n\n" + "", value = "search DPO")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(method = RequestMethod.POST, value = "/searchDPO/{longResultFlag}")
	public ResponseEntity<List<String>> searchDPO(@PathVariable boolean longResultFlag,
			@RequestBody @ApiParam(defaultValue = "hello") String searchString) {
		List<String> searchResult = new ArrayList<String>();
		HttpStatus httpstatus;
		ResponseEntity<List<String>> response;

		// TODO validate the search string

		DPOQuery configuredQuery = ontology.configureQuery(searchString);

		if (configuredQuery.isValid()) {
			try {
				searchResult = dpos.advancedSearchDPO(configuredQuery, longResultFlag);
				httpstatus = HttpStatus.OK;
			} catch (Exception e) {
				// TODO: clean up the way errors are handled.
				searchResult = new ArrayList<String>();
				searchResult.add("Error running search");
				httpstatus = HttpStatus.NOT_FOUND;
				e.printStackTrace();
			}
			response = new ResponseEntity<List<String>>(searchResult, httpstatus);
		} else {
			httpstatus = HttpStatus.BAD_REQUEST;
			// TODO: clean up the way errors are handled.
			List<String> errors = new ArrayList<String>();
			errors.add(configuredQuery.getValidationErrorsAsString());
			response = new ResponseEntity<List<String>>(errors, httpstatus);
		}
		return response;
	}

	/**
	 * readDPO
	 * <p>
	 * Retrieves the data protected object identified by dpoId
	 * </p>
	 *
	 * @param dpoId the unique ID of the DPO to be retrieved
	 * @return an object encapsulating the four CTI Bundle components in the DPOS:
	 *         <p>
	 *         {<BR>
	 *         dpo_metadata: &lt;byte[]&gt;,<BR>
	 *         cti: &lt;byte[]&gt;,<BR>
	 *         dsa:&lt;byte[]&gt;,<BR>
	 *         hash: &lt;byte[]&gt;<BR>
	 *         }
	 *         </p>
	 *
	 */
	@ApiOperation(notes = "Retrieves the DPO identified by its id. ", value = "read DPO")
	@ApiResponses(value = {
			// @ApiResponse(code = 302, message = "Found"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(method = RequestMethod.GET, value = "/readDPO/{dpoId}")
	public ResponseEntity<DPO> readDPO(@PathVariable String dpoId) {
		ResponseEntity<DPO> response;
		try {
			DPO dpo = dpos.readDPO(dpoId);
			if (dpo.getMetadata() != null) {
				response = new ResponseEntity<DPO>(dpo, HttpStatus.OK);
			} else {
				response = new ResponseEntity<DPO>(dpo, HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			response = new ResponseEntity<DPO>(new DPO(), HttpStatus.NOT_FOUND);
		}
		return response;
	}

	/**
	 * deleteDPO
	 * <p>
	 * Delete a DPO by its identifier.
	 * </p>
	 *
	 * @param dpoId the identifier of the DPO to be deleted
	 * @return a success message or error message
	 */
	@ApiOperation(notes = "Delete a DPO by its identifier.", value = "delete DPO")
	@ApiResponses(value = {
			// @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 204, message = "Content Not Found"), @ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteDPO/{dpoId}")
	public ResponseEntity<String> deleteDPO(@PathVariable String dpoId) {
		HttpStatus httpstatus;
		String resp;
		ResponseEntity<String> response;
		try {
			dpos.deleteDPO(dpoId);
			LOGGER.info("delete the DPO " + dpoId);
			resp = "Delete successful.";
			httpstatus = HttpStatus.OK;
		} catch (Exception e) {
			LOGGER.error("Exception message is: " + e.getMessage());
			resp = "Error deleting DPO. " + e.getMessage();
			httpstatus = HttpStatus.NOT_FOUND;
		}
		response = new ResponseEntity<String>(resp, httpstatus);
		return response;
	}

}
